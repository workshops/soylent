# Soylent - cyberfood for digital dystopia

Soylent aims to provide all human nutrition in one powder.

To function, humans need carbon hydrates, fats, proteins, and certain vitamins.

There is a company by some techbro who first made it popular. Because it's not
shipped in Europe, I found a recipe online and mixed it myself.

there are many recipes, mine was optimized for:
* cheap (1,33€ a day for me)
* vegan
* widely available ingredients (I only had to go to one online shop and 2 stores)
* few ingredients (only 7 or 8 different ingredients)

You can create your own recipes on https://www.completefoods.co/

My favourite recipe has 8 ingredients, but does not really fulfill all nutrition:
* 150g oat flakes
* 150g sugar
* 110g soy flakes
* 4g iodine salt
* 1.5 pills with diverse vitamines
* 30ml sunflower oil
* 50g pea protein
* 70g wheat flour
* has not enough Calcium, Vitamin A & D, and Choline.

You just add the powder into a cup, add the oil, add water, and drink it/eat it
with a spoon.

## advantages

tenable - does not go bad, you can store it for months, maybe years

you don't have to cook (when you don't want to)
* preparing does not take much time (around 20 minutes for 2 days)

Low water usage

you don't have to clean many dishes

no organic waste, (almost) nothing is thrown away

you can buy and store always the same stuff

you don't need a fridge, a stove, nor an oven

eating real food is more special and tastes better

gaining or losing weight is easier, you just eat more or less

## experiences

after 8 months, I did not have enough iron, that was basically the only bad
consequence

people would give me food all the time, because they were worried

I think I never went more than 2-3 days without eating anything else

## criticism

it does not taste very well
* but also not very bad. mine actually tastes a bit sweet, with a consistency
  like cereals.

bodies are complex and intransparent - not just input-output-machines
* we don't know how specific bodies process food - e.g. iron processing
* we don't know how bodies change when they eat specific food all the time

focus on function over anything else, alienation from own body
* alienation is not necessarily bad
* but you are not a machine, you have to decide yourself if you want to become
  similar to one

There is a lot of weird criticism, because food is sacred. I broke already
many social norms, but few are met with as much weird looks as food hacking.
So you will meet people who will feel unsettled, but don't know how. Even I
felt really weird, when I prepared myself to eat the first portion: but soon it
turned into being satisfied :D

